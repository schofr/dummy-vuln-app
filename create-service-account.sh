#!/bin/bash
#
API_TOKEN=sysdig-secure-api-token
#
USER="service-n49k@sysdig.com"
#
USER_JSON='{"username": "'$USER'"}'
#
curl -s -o user-result.json -H "Content-Type: application/json" -H "Authorization: Bearer ${API_TOKEN}" 'https://secure.sysdig.com/api/user/provisioning/' -d "${USER_JSON}"
#
USER_TOKEN=$(jq < user-result.json .token.key)
#
echo
echo "Username: ${USER}"
echo "User token: ${USER_TOKEN}"
